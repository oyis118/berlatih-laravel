<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('form');
    }
    public function success(Request $request){
        $namaf = $request->name1;
        $namas = $request->name2;
        return view('welcome', compact('namaf', 'namas'));
    }
}
